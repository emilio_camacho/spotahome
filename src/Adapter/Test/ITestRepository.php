<?php

namespace App\Adapter\Test;


interface ITestRepository
{
    public function store(string $userName);
}