<?php

namespace App\Converters;


use App\Entity\Property;

class Properties
{
    public static function convert(\SimpleXMLElement $data): Array {
        $properties = [];
        foreach($data->ad as $propertyXml) {
            $properties[] = new Property(
                (int) $propertyXml->id,
                (string) $propertyXml->url,
                (string) $propertyXml->title,
                (string) $propertyXml->city,
                (string) $propertyXml->pictures[0]->picture_url
            );
        }
        return $properties;
    }
}