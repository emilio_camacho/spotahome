<?php

namespace App\UseCases;

/**
 * Interface IUseCase
 * @package App\UseCases
 */
interface IUseCase
{
    public function execute(IUseCaseRequest $request);
}