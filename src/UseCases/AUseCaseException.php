<?php

namespace App\UseCases;


class AUseCaseException extends \Exception
{
    /**
     * Internal use case related exception code
     * @var int
     */
    private $internalCode;

    /**
     * @param string $internalCode
     * @param int $message
     */
    public function __construct(int $internalCode, string $message)
    {
        parent::__construct($message);
        $this->internalCode = $internalCode;
    }

    /**
     * @return int
     */
    public function getInternalCode()
    {
        return $this->internalCode;
    }

    /**
     * @param int $internalCode
     */
    public function setInternalCode(int $internalCode)
    {
        $this->internalCode = $internalCode;
    }
}