<?php

namespace App\UseCases\Source\Get;


use App\Entity\User;
use App\UseCases\IUseCaseResponse;

class Response implements IUseCaseResponse
{
    private $_data;

    public function __construct(Array $data = null)
    {
        $this->_data = $data;
    }

    /**
     * @return array
     */
    public function getData(): array
    {
        return $this->_data;
    }



}