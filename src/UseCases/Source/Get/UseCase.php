<?php

namespace App\UseCases\Source\Get;


use App\Converters\Properties;
use \Exception as exc;

class UseCase
{
    public function execute(Request $request): Response
    {

        try {
            $data = simplexml_load_file($request->getPath());
            $properties = Properties::convert($data);
            return new Response($properties);
        } catch (exc $e) {
            throw new Exception(
                Exception::PATH_NOT_FOUND,
                $e->getMessage()
            );
        }


    }

}