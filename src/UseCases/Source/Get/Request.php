<?php

namespace App\UseCases\Source\Get;


use App\UseCases\IUseCaseRequest;

class Request implements IUseCaseRequest
{
    private $_path;

    public function __construct(string $_path)
    {
        $this->_path = $_path;
    }

    public function getPath(): string
    {
        return $this->_path;
    }

}