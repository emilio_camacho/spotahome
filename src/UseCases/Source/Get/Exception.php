<?php
namespace App\UseCases\Source\Get;

use App\UseCases\AUseCaseException;

class Exception extends AUseCaseException {
    const PARAMETER_REQUIRED = 1;
    const PATH_NOT_FOUND = 2;
}