<?php

namespace App\UseCases\Source\Order;


use App\Converters\Properties;
use App\Entity\Property;
use App\Repository\UserRepository;
use \Exception as exc;

class UseCase
{
    const FIELD_ID = "ID";
    const FIELD_TITLE = "TITLE";
    const FIELD_LINK = "LINK";
    const FIELD_CITY = "CITY";
    const FIELD_PICTURE = "PICTURE";


    const ASC = "ASC";
    const DESC = "DESC";

    public function execute(Request $request): Response
    {
        $properties = $request->getProperties();
       switch ($request->getField()) {
           case self::FIELD_ID:
                    if ($request->getOrder() === self::ASC) {
                        usort(
                            $properties,
                            array($this,"sortByIdAsc")
                        );
                    } else {
                        usort(
                            $properties,
                            array($this,"sortByIdDesc")
                        );
                    }
                break;
           case self::FIELD_TITLE:
               if ($request->getOrder() === self::ASC) {
                   usort(
                       $properties,
                       array($this,"sortByTitleAsc")
                   );
               } else {
                   usort(
                       $properties,
                       array($this,"sortByTitleDesc")
                   );
               }
                break;

           case self::FIELD_LINK:
               if ($request->getOrder() === self::ASC) {
                   usort(
                       $properties,
                       array($this,"sortByLinkAsc")
                   );
               } else {
                   usort(
                       $properties,
                       array($this,"sortByLinkDesc")
                   );
               }
                break;

           case self::FIELD_CITY:
               if ($request->getOrder() === self::ASC) {
                   usort(
                       $properties,
                       array($this,"sortByCityAsc")
                   );
               } else {
                   usort(
                       $properties,
                       array($this,"sortByCityDesc")
                   );
               }
                break;

           case self::FIELD_PICTURE:
               if ($request->getOrder() === self::ASC) {
                   usort(
                       $properties,
                       array($this,"sortByPictureAsc")
                   );
               } else {
                   usort(
                       $properties,
                       array($this,"sortByPictureDesc")
                   );
               }
                break;
       }
       return new Response($properties);
    }

    private function sortByIdAsc(Property $propertyA, Property $propertyB)
    {
        return $propertyA->getId() >= $propertyB->getId();
    }

    private function sortByIdDesc(Property $propertyA, Property $propertyB)
    {
        return !$this->sortByIdAsc($propertyA, $propertyB);
    }

    private function sortByTitleAsc(Property $propertyA, Property $propertyB)
    {
        return $propertyA->getTitle() >= $propertyB->getTitle();
    }

    private function sortByTitleDesc(Property $propertyA, Property $propertyB)
    {
        return !$this->sortByTitleAsc($propertyA, $propertyB);
    }

    private function sortByLinkAsc(Property $propertyA, Property $propertyB)
    {
        return $propertyA->getUrl() >= $propertyB->getUrl();
    }

    private function sortByLinkDesc(Property $propertyA, Property $propertyB)
    {
        return !$this->sortByLinkAsc($propertyA, $propertyB);
    }

    private function sortByCityAsc(Property $propertyA, Property $propertyB)
    {
        return $propertyA->getCity() >= $propertyB->getCity();
    }

    private function sortByCityDesc(Property $propertyA, Property $propertyB)
    {
        return !$this->sortByCityAsc($propertyA, $propertyB);
    }

    private function sortByPictureAsc(Property $propertyA, Property $propertyB)
    {
        return $propertyA->getPicture() >= $propertyB->getPicture();
    }

    private function sortByPictureDesc(Property $propertyA, Property $propertyB)
    {
        return !$this->sortByPictureAsc($propertyA, $propertyB);
    }

}