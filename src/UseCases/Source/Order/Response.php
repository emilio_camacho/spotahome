<?php

namespace App\UseCases\Source\Order;


use App\Entity\User;
use App\UseCases\IUseCaseResponse;

class Response implements IUseCaseResponse
{
    private $_data;

    public function __construct(Array $data = null)
    {
        $this->_data = $data;
    }

    public function getData(): array
    {
        return $this->_data;
    }



}