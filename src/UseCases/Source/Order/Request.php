<?php

namespace App\UseCases\Source\Order;


use App\UseCases\IUseCaseRequest;

class Request implements IUseCaseRequest
{
    private $_field;
    private $_order;
    private $_properties;


    public function __construct(string $field, string $order, array $properties)
    {
        $this->_field = $field;
        $this->_order = $order;
        $this->_properties = $properties;
    }

    public function getOrder(): string
    {
        return $this->_order;
    }

    public function getField(): string
    {
        return $this->_field;
    }


    public function getProperties(): array
    {
        return $this->_properties;
    }


}