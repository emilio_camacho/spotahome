<?php

namespace App\Entity;


class Property
{
    private $_id;
    private $_url;
    private $_title;
    private $_city;
    private $_picture;


    public function __construct(
        int $_id,
        string $_url,
        string $_title,
        string $_city,
        string $_picture
    ){
        $this->_id = $_id;
        $this->_url = $_url;
        $this->_title = $_title;
        $this->_city = $_city;
        $this->_picture = $_picture;
    }

    public function getId():int
    {
        return $this->_id;
    }


    public function getUrl():string
    {
        return $this->_url;
    }


    public function getTitle():string
    {
        return $this->_title;
    }

    public function getCity():string
    {
        return $this->_city;
    }

    public function getPicture():string
    {
        return $this->_picture;
    }




}

