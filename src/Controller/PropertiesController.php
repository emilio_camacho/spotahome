<?php

namespace App\Controller;

use App\UseCases\Source\Get\{UseCase as UseCaseGet, Request as RequestGet};
use App\UseCases\Source\Order\{UseCase as UseCaseOrder, Request as RequestOrder};
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PropertiesController extends Controller
{

    private $_useCaseGet;
    private $_useCaseOrder;


    public function __construct(
        UseCaseGet $_useCaseGet,
        UseCaseOrder $_useCaseOrder
    ){
        $this->_useCaseGet = $_useCaseGet;
        $this->_useCaseOrder = $_useCaseOrder;
    }


    /**
     * @Route("/properties/{field}/{order}", name="properties", )
     */
    public function index(string $field = "ID", string $order = "ASC")
    {
        $orderedProperties = $this->getOrderedProperties($field, $order);

        return $this->render('default/properties.html.twig', [
            'properties' => $orderedProperties,
            'field' => $field,
            'order' => $order,
        ]);
    }

    /**
     * @Route("/download/{field}/{order}", name="download", )
     */
    public function download(string $field = "ID", string $order = "ASC")
    {
        $orderedProperties = $this->getOrderedProperties($field, $order);
        return $this->json(
            $orderedProperties,
            200,
            [
                'Content-Disposition' =>  'attachment',
            ]
        );
    }

    private function getOrderedProperties(
        string $field = "ID",
        string $order = "ASC"
    ): array
    {
        $request = new RequestGet(getenv("PATH_XML"));
        $response = $this->_useCaseGet->execute($request);
        $properties = $response->getData();

        $requestOrder = new RequestOrder($field, $order, $properties);
        $responseOrder = $this->_useCaseOrder->execute($requestOrder);
        return $responseOrder->getData();
    }
}