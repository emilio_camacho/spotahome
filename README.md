#PHP backend developer test for Sportahome
> Application for spotahome where you can order a list of properties by any of their fields, 
you can also download the list ordered in json.
## Requeriments:
* [Docker](https://www.docker.com)

## Set up:
* ```git clone https://emilio_camacho@bitbucket.org/emilio_camacho/spotahome.git ```
* ```cd spotahome```
* ```docker-compose build```
* ```docker-compose up -d```
* ```docker exec -it spotahome-php bash```
* ```composer update```

## How to run:
* ```docker exec -it spotahome-php bash```
* ```php -S 0.0.0.0:8080 -t /app/public /app/public/index.php > phpd.log 2>&1 &```
* Go to [http://localhost:8080/properties](http://localhost:8080/properties)

## Testing:
* ```docker exec -it spotahome-php bash```
* ```phpunit```

## Considerations:
* Make sure port 8080 is free in your system 
* Each time you run ```docker exec -it spotahome-php bash``` your are getting access to the terminal withing
the container, use ```exit``` to back to your own terminal.
* By default the app run in dev environment, this will load the debug symfony tool making the app slower, you can change
to ```APP_ENV=prod``` in .env to run it without the debug tool.